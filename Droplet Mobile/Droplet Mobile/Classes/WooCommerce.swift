//
//  WooCommerce.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 03/11/21.
//

import Foundation
import Alamofire
import SwiftyJSON

final class WooCommerce{
    
    let user = "ck_90255c5acbcb24a331615bfbdf49e861c1a4c6e2"
    let password = "cs_9df9c8eaee91abf9fd8eb5a45c7dd0798cca6770"
    static let shared = WooCommerce()
    
    public func createUser(userWooCommerce: User, completion: @escaping((Int?) -> Void)){
        DispatchQueue.main.async {
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/customers?consumer_key=\(self.user)&consumer_secret=\(self.password)", method: .post, parameters: userWooCommerce, encoder: JSONParameterEncoder.default)
                .authenticate(username: self.user, password: self.password)
                .responseJSON{ response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        completion(json["id"].int)
                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
        }
    }
    
    public func updateUser(userWooCommerce: User, id: Int, completion: @escaping((String?) -> Void)){
        DispatchQueue.main.async {
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/customers/\(id)?consumer_key=\(self.user)&consumer_secret=\(self.password)", method: .post, parameters: userWooCommerce, encoder: JSONParameterEncoder.default)
                .authenticate(username: self.user, password: self.password)
                .responseJSON{ response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        completion(json["username"].string)
                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
        }
    }
    
    public func createOrder(order: Order, completion: @escaping((String?) -> Void)){
        DispatchQueue.main.async {
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/orders?consumer_key=\(self.user)&consumer_secret=\(self.password)", method: .post, parameters: order, encoder: JSONParameterEncoder.default)
                .authenticate(username: self.user, password: self.password)
                .responseJSON{ response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        completion(json["status"].string)
                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
        }
    }
}
