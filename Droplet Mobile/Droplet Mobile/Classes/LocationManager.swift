//
//  LocationManager.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    
    let manager = CLLocationManager()
    
    var completion: ((CLLocation) -> Void)?
    
    public func getUserLocation(completion: @escaping ((CLLocation) -> Void)){
        self.completion = completion
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
        manager.startUpdatingLocation()
    }
    
    public func resolveLocationName(with location:CLLocation, completion: @escaping((String?) -> Void)){
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, preferredLocale: .current) { placemarks, error in
            guard let place = placemarks?.first, error == nil else{
                completion(nil)
                return
            }
            
            var name = ""
            if let street = place.thoroughfare{
                name += street
            }
            if let streetNumber = place.subThoroughfare{
                name += " \(streetNumber)"
            }
            if let locality = place.locality{
                name += ", \(locality)"
            }
            if let state = place.administrativeArea{
                name += ", \(state)"
            }
            if let postalCode = place.postalCode{
                name += ", \(postalCode)"
            }
            completion(name)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else{
            return
        }
        completion?(location)
        manager.stopUpdatingLocation()
    }
}
