//
//  Profile.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 04/10/21.
//

import Foundation
import FirebaseFirestore

class Profile{
    var firstName: String?
    var lastName: String?
    var email: String?
    var phone: String?
    
    private let db = Firestore.firestore()
    
    init() {
        let defaults = UserDefaults.standard
        self.phone = defaults.value(forKey: "phone") as? String
        
        if let firstName = defaults.value(forKey: "firstName") as? String, let lastName = defaults.value(forKey: "lastName") as? String, let email = defaults.value(forKey: "email") as? String{
            self.firstName = firstName
            self.lastName = lastName
            self.email = email
        }else{
            db.collection("users").document(self.phone!).getDocument{(documentSnapshot, error) in
                if let document = documentSnapshot, error == nil{
                    if let nombre = document.get("firstName") as? String{
                        self.firstName = nombre
                    }
                    if let apellido = document.get("lastName") as? String{
                        self.firstName = apellido
                    }
                    if let email = document.get("email") as? String{
                        self.email = email
                    }
                }
            }
        }
    }
    
    func saveProfile(){
        // Guardamos los datos del usuario
        let defaults = UserDefaults.standard
        defaults.set(self.firstName, forKey: "firstName")
        defaults.set(self.lastName, forKey: "lastName")
        defaults.set(self.email, forKey: "email")
        defaults.set(self.phone, forKey: "phone")
        defaults.synchronize()
    }
    
//    func createOrder() {
//        DispatchQueue.main.async {
//            let user = "ck_3a2787bf6160932511414e21f6f3d564f9e8d155"
//            let password = "cs_2da22a81fd1c2bc8f91523e5d69757d9d4fb01da"
//            let jsonObject: [String: Any] = [
//                "payment_method": "bacs",
//                "payment_method_title": "Direct Bank Transfer",
//                "set_paid": true,
//                "billing": [
//                    "first_name": "John",
//                    "last_name": "Doe",
//                        "company": "",
//                        "address_1": "969 Market",
//                        "address_2": "",
//                        "city": "San Francisco",
//                        "state": "CA",
//                        "postcode": "94103",
//                        "country": "US",
//                        "email": "john.doe@example.com",
//                        "phone": "(555) 555-5555"
//                ],
//                "shipping": [
//                    "first_name": "John",
//                    "last_name": "Doe",
//                    "address_1": "969 Market",
//                    "address_2": "",
//                    "city": "San Francisco",
//                    "state": "CA",
//                    "postcode": "94103",
//                    "country": "US"
//                ],
//                "line_items":[
//                    ["product_id": 93,
//                    "quantity": 2],
//                    ["product_id": 22,
//                    "variation_id": 23,
//                    "quantity": 1]
//                ],
//                "shipping_lines": [
//                    "method_id": "flat_rate",
//                    "method_title": "Flat Rate",
//                    "total": "10.00"
//                ]
//            ]
//            AF.request("https://www.droplet.delivery/wp-json/wc/v3/orders?consumer_key=\(user)&consumer_secret=\(password)", method: .post, parameters: JSONSerialization.isValidJSONObject(jsonObject), encoder: JSONParameterEncoder.default)
//                .authenticate(username: user, password: password)
//                .responseJSON{ response in
//                    switch response.result{
//                    case .success(let value):
//                        let json = JSON(value)
//                        debugPrint(json)
//                    case .failure(let error):
//                        print(error)
//                    }
//                    
//                }
//        }
//    }
//    
//    func createOrderV2(){
//        //let defaults = UserDefaults.standard
//        let billing = Billing(first_name: "John", last_name: "Doe", company: "", address_1: "969 Market", address_2: "", city: "San Francisco", state: "CA", postcode: "94103", country: "US", email: "john.doe@example.com", phone: "(555) 555-5555")
//        //defaults.set(billing, forKey: "billing")
//        let shipping = Shipping(first_name: "John", last_name: "Doe", company: "", address_1: "969 Market", address_2: "", city: "San Francisco", state: "CA", postcode: "94103", country: "US")
//        //defaults.set(shipping, forKey: "shipping")
//        let pure = Product(product_id: 134, name: "PURE + ELECTROLYTES", description: "PURE + ELECTROLYTES (12 BOTTLES)", quantity: 1, cost: 22.0, stock_status: "instock")
//        let lineItems = [pure]
//        let shipingLines = [
//            "method_id": "flat_rate",
//            "method_title": "Flat Rate",
//            "total": "10.00"
//        ]
//        let order = Order(payment_method: "bacs", payment_method_title: "Direct Bank Transfer", set_paid: true, billing: billing, shipping: shipping, line_items: lineItems, shipping_lines: shipingLines)
//        
//        
//        let jsonEncoder = JSONEncoder()
//        jsonEncoder.outputFormatting = .prettyPrinted
//
//        do {
//            let encodePerson = try jsonEncoder.encode(order)
//            let endcodeStringPerson = String(data: encodePerson, encoding: .utf8)!
//            print(endcodeStringPerson)
//            print(JSONSerialization.isValidJSONObject(encodePerson))
//        } catch {
//            print(error.localizedDescription)
//        }
//        
//        //defaults.synchronize()
//    }
}
