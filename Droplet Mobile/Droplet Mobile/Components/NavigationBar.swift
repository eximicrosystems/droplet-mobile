//
//  ViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 24/09/21.
//

import UIKit
import Popover

class NavigationBar: UIView {
    
    
    @IBOutlet var deliverNowlbl: UILabel!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var moreAddressButton: UIButton!
    @IBOutlet var iconNavBar: UIImageView!
    @IBOutlet var sectionNavBar: UILabel!
    private var imageName: String!

    init(frame: CGRect, imageName: String?) {
        self.imageName = imageName
        super.init(frame: frame)
        commonInit()
        typeNavBar()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("NavigationBar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        getCurrentLocation()
    }
    
    func typeNavBar() {
        if self.imageName != nil {
            deliverNowlbl.isHidden = true
            addressLbl.isHidden = true
            moreAddressButton.isHidden = true
            iconNavBar.isHidden = false
            switch self.imageName {
            case "account":
                iconNavBar.image = UIImage(named: "ACCOUNT-ICON-1")
            case "orders":
                iconNavBar.image = UIImage(named: "ORDERS-ICON-1")
            case "support":
                iconNavBar.image = UIImage(named: "SUPPORT-ICON-1")
            case "partner":
                iconNavBar.image = UIImage(named: "WELLNESS-PARTNER-ICON-BLUE")
                imageName = ""
            case "brands":
                iconNavBar.image = UIImage(named: "DROPLET-TRUCK")
                imageName = ""
            default:
                print("No se encontró el icono")
            }
            sectionNavBar.isHidden = false
            sectionNavBar.text = self.imageName.uppercased()
        }
    }
    
    func getCurrentLocation(){
        LocationManager.shared.getUserLocation { [weak self] location in
            DispatchQueue.main.async {
                LocationManager.shared.resolveLocationName(with: location) { locationName in
                    self!.addressLbl.text = locationName?.uppercased()
                    //self!.moreAddressButton.frame.origin.x += 78
                    
                    let defaults = UserDefaults.standard
                    defaults.set(locationName?.uppercased(), forKey: "address")
                    defaults.synchronize()
                }
            }
        }
    }
    
    @IBAction func addressButton(_ sender: Any) {
        //self.moreAddressButton.frame.origin.x = 251
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:220))
        let addressView = AddAddressView(frame: CGRect(x: 0, y: 10, width: aView.frame.width, height: 200))
        addressView.navBar = self
        let popoverView = Popover(options: nil, showHandler: nil, dismissHandler: nil)
        addressView.popUp = popoverView
        aView.addSubview(addressView)
        popoverView.show(aView, fromView: self.moreAddressButton)
    }
    
    @IBAction func lateralMenuButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
    
}
