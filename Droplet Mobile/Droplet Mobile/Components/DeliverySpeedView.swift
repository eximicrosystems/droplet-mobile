//
//  DeliverySpeedView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 13/08/21.
//

import UIKit
import Popover

class DeliverySpeedView: UIView {
    
    @IBOutlet var hour: UIButton!
    @IBOutlet var hours: UIButton!
    @IBOutlet var sameDay: UIButton!
    private var pop: Popover!
    private var yourbox: YourBoxMenu!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        hour.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("DeliverySpeedView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func setPop(pop: Popover) {
        self.pop = pop
    }
    
    func setBox(box: YourBoxMenu) {
        self.yourbox = box
    }
    
    @IBAction func closeButton(_ sender: Any) {
        pop.dismiss()
    }
    
    @IBAction func hourButton(_ sender: Any) {
        hour.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        hours.setImage(UIImage(systemName: "circle"), for: .normal)
        sameDay.setImage(UIImage(systemName: "circle"), for: .normal)
        self.yourbox.deliverySpeed.text = "WHITIN 1 HOUR"
        self.yourbox.deliveryCost.text = "$15.95"
        self.yourbox.deliveryfee.text = "15.95"
        self.yourbox.updateTotal()
    }
    @IBAction func hoursButton(_ sender: Any) {
        hour.setImage(UIImage(systemName: "circle"), for: .normal)
        hours.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        sameDay.setImage(UIImage(systemName: "circle"), for: .normal)
        self.yourbox.deliverySpeed.text = "WHITIN 4 HOURS"
        self.yourbox.deliveryCost.text = "$12.95"
        self.yourbox.deliveryfee.text = "12.95"
        self.yourbox.updateTotal()
    }
    @IBAction func sameDayButton(_ sender: Any) {
        hour.setImage(UIImage(systemName: "circle"), for: .normal)
        hours.setImage(UIImage(systemName: "circle"), for: .normal)
        sameDay.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        self.yourbox.deliverySpeed.text = "SAME DAY DELIVERY"
        self.yourbox.deliveryCost.text = "$7.95"
        self.yourbox.deliveryfee.text = "7.95"
        self.yourbox.updateTotal()
    }
    
}
