//
//  AddAddressView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 11/08/21.
//

import Foundation
import UIKit
import CoreLocation
import Popover

class AddAddressView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var directionTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var currentLocationLbl: UILabel!
    @IBOutlet var deliveryAddressLbl: UILabel!
    
    private var places: [Place] = []
    var navBar: NavigationBar!
    var yourBox: YourBoxMenu!
    var popUp: Popover!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        saveButton.layer.borderColor = #colorLiteral(red: 0.05609246343, green: 0.3262491822, blue: 0.8220757842, alpha: 1)
        saveButton.layer.borderWidth = 2
        getCurrentLocation()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("AddAddressView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        directionTextField.addTarget(self, action: #selector(searching), for: .editingChanged)
    }
    func getCurrentLocation(){
        LocationManager.shared.getUserLocation { [weak self] location in
            DispatchQueue.main.async {
                LocationManager.shared.resolveLocationName(with: location) { locationName in
                    self?.currentLocationLbl.text = locationName?.uppercased()
                }
            }
        }
    }
    @objc fileprivate func searching(){
        print(directionTextField.text!)
        guard let query = directionTextField.text,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else{
            return
        }
        GooglePlacesManager.shared.findPlaces(query:query){ result in
            switch result{
            case .success(let places):
                print("found results")
                DispatchQueue.main.async {
                    self.update(with: places)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    public func update(with places: [Place]){
        self.places = places
        tableView.isHidden = false
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.white
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = places[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        directionTextField.text = places[indexPath.row].name
        tableView.isHidden = true
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        if navBar != nil {
            self.navBar.addressLbl.text = directionTextField.text
//            self.navBar.moreAddressButton.frame.origin.x += 40
        }else if yourBox != nil{
            self.yourBox.addressLbl.text = directionTextField.text
        }
        
        if let address = directionTextField.text {
            let defaults = UserDefaults.standard
            defaults.set(address, forKey: "address")
            defaults.synchronize()
        }
        
        self.popUp.dismiss()
    }
    @IBAction func setCurrentLocationAction(_ sender: Any) {
        directionTextField.text = currentLocationLbl.text
    }
    @IBAction func setDeliveryAddressAction(_ sender: Any) {
        directionTextField.text = deliveryAddressLbl.text
    }
}
