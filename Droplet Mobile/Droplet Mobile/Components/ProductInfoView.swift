//
//  ProductInfoView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 11/08/21.
//

import Foundation
import UIKit

class ProductInfoView: UIView {
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var pureWaterView: UIView!
    @IBOutlet var pureWaterInfoView: UIView!
    @IBOutlet var alkalineWaterView: UIView!
    @IBOutlet var alkalineWaterInfoView: UIView!
    
    var type: Types
    
    init(frame: CGRect, types: Types) {
        type = types
        super.init(frame: frame)
        commonInit()
        displayViews()
        self.layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("ProductInfoView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func displayViews(){
        switch type {
        case .water:
            pureWaterView.isHidden = false
            pureWaterInfoView.isHidden = true
        case .alkaline:
            alkalineWaterView.isHidden = false
            alkalineWaterInfoView.isHidden = true
        }
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func rightButton(_ sender: Any) {
        pageControl.currentPage += 1
        switch type {
        case .water:
            if pageControl.currentPage == 0 {
                pureWaterView.isHidden = false
                pureWaterInfoView.isHidden = true
            }else{
                pureWaterView.isHidden = true
                pureWaterInfoView.isHidden = false
            }
        case .alkaline:
            if pageControl.currentPage == 0 {
                alkalineWaterView.isHidden = false
                alkalineWaterInfoView.isHidden = true
            }else{
                alkalineWaterView.isHidden = true
                alkalineWaterInfoView.isHidden = false
            }
        }
        
    }
    @IBAction func leftButton(_ sender: Any) {
        pageControl.currentPage -= 1
        switch type {
        case .water:
            if pageControl.currentPage == 0 {
                pureWaterView.isHidden = false
                pureWaterInfoView.isHidden = true
            }else{
                pureWaterView.isHidden = true
                pureWaterInfoView.isHidden = false
            }
        case .alkaline:
            if pageControl.currentPage == 0 {
                alkalineWaterView.isHidden = false
                alkalineWaterInfoView.isHidden = true
            }else{
                alkalineWaterView.isHidden = true
                alkalineWaterInfoView.isHidden = false
            }
        }
    }
    
}
