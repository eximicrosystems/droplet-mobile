//
//  YourBoxTableViewCell.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 01/10/21.
//

import UIKit
import DropDown

protocol MyTableViewCellDelegate: AnyObject {
    func didTabButton()
}

class YourBoxTableViewCell: UITableViewCell {

    @IBOutlet var concept: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var quantityButton: UIButton!
    @IBOutlet var dropdownQuantity: UIView!
    
    private var title: String = ""
    private var type: String = ""
    private var index: Int = 0
    private var priceW: String = "0"
    weak var delegate: MyTableViewCellDelegate?
    var yourBox: YourBoxMenu!
    static let identifier = "YourBoxTableViewCell"
    
    let dropDown = DropDown()
    let quantityData = ["1","2","3","4","5","6","7","8","9","10"]
    
    static func nib() -> UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addDropdown()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func selectRow(){
        delegate?.didTabButton()
    }
    
    func configure(title: String, quantity: Int, price: String, type: String, showButton: Bool){
        concept.text = title
        self.priceW = price
        self.type = type
        self.price.text = "$\(Int(price)!*quantity).00"
        self.quantity.text = "\(quantity)"
        quantityButton.isHidden = showButton
        
    }
    
    func addDropdown(){
        dropDown.anchorView = dropdownQuantity
        dropDown.dataSource = quantityData
        // Top of drop down will be below the anchorView
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            quantity.text = item
            if let price = self.priceW as? String, let quantity = self.quantity.text{
                self.price.text = "$\(Int(price)!*Int(quantity)!).00"
            }
            
            if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
                var items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
                if self.type == "pure"{
                    if var pure = items![1] as? Product{
                        pure.quantity = Int(item)!
                        items![1] = pure
                    }
                }else{
                    if var alkaline = items![0] as? Product{
                        alkaline.quantity = Int(item)!
                        items![0] = alkaline
                    }
                }
                let defaults = UserDefaults.standard
                defaults.set(try? PropertyListEncoder().encode(items), forKey: "items")
                defaults.synchronize()
            }
            selectRow()
        }
        dropDown.width = 50
    }
    
    @IBAction func selectQuantity(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        print("Delete product")
        if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
            var items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
            if self.type == "pure"{
                if var pure = items![1] as? Product{
                    pure.quantity = 0
                    items![1] = pure
                }
            }else{
                if var alkaline = items![0] as? Product{
                    alkaline.quantity = 0
                    items![0] = alkaline
                }
            }
            let defaults = UserDefaults.standard
            defaults.set(try? PropertyListEncoder().encode(items), forKey: "items")
            defaults.synchronize()
        }
        selectRow()
        self.removeFromSuperview()
    }
}
