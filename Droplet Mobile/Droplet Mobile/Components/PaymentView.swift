//
//  PaymentViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 12/10/21.
//

import UIKit
import SVProgressHUD

class PaymentView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("PaymentView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }

    @IBAction func paypalPayment(_ sender: Any) {
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            SVProgressHUD.dismiss()
            self.removeFromSuperview()
            if let topVC = UIApplication.getTopViewController() {
                topVC.navigationController?.pushViewController(ConfirmedViewController(), animated: false)
            }
        })
    }
    
    @IBAction func cardPayment(_ sender: Any) {
        SVProgressHUD.show()
        self.removeFromSuperview()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            SVProgressHUD.dismiss()
            if let topVC = UIApplication.getTopViewController() {
                topVC.navigationController?.pushViewController(ConfirmedViewController(), animated: false)
            }
        })
    }
    
    @IBAction func applePayment(_ sender: Any) {
        SVProgressHUD.show()
        self.removeFromSuperview()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            SVProgressHUD.dismiss()
            if let topVC = UIApplication.getTopViewController() {
                topVC.navigationController?.pushViewController(ConfirmedViewController(), animated: false)
            }
        })
    }
    
    @IBAction func closeButton(_ sender: Any) {
        removeFromSuperview()
    }
}
