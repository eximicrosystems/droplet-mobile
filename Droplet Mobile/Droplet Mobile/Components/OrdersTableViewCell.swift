//
//  OrdersTableViewCell.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 08/11/21.
//

import UIKit

class OrdersTableViewCell: UITableViewCell {

    @IBOutlet var date: UILabel!
    @IBOutlet var status: UILabel!
    @IBOutlet var amount: UILabel!
    
    private var title: String = ""
    private var index: Int = 0
    
    static let identifier = "OrdersTableViewCell"
    
    static func nib() -> UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        
    func configure(date: String, status: String, amount: String){
        self.date.text = date
        self.status.text = status.uppercased()
        self.amount.text = "$\(amount)"
    }
    
    @IBAction func showInfoButton(_ sender: Any) {
        print("Se pulso el botón")
    }
}
