//
//  LateralMenu.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 28/07/21.
//

import Foundation
import UIKit
import FirebaseAuth

class LateralMenu: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("LateralMenu", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    @IBAction func homeButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(HomeViewController(), animated: false)
        }
    }
    @IBAction func accountButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(AccountViewController(), animated: false)
        }
    }
    @IBAction func ordersButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(NewOrdersViewController(), animated: false)
        }
    }
    @IBAction func supportButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(SupportViewController(), animated: false)
        }
    }
    @IBAction func wellnessButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(PartnerViewController(), animated: false)
        }
    }
    @IBAction func brandsButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(BrandsViewController(), animated: false)
        }
    }
    @IBAction func privacyButton(_ sender: Any) {
        self.removeFromSuperview()
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(PrivacyViewController(), animated: false)
        }
    }
    @IBAction func logoutButton(_ sender: Any) {
        self.removeFromSuperview()
        firebaseLogout()
    }
    
    @IBAction func closeMenu(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    func firebaseLogout(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "phoneNumber")
        do{
            try Auth.auth().signOut()
            if let topVC = UIApplication.getTopViewController() {
                topVC.navigationController?.pushViewController(LoginViewController(), animated: false)
            }
        }catch{
            print("ERROR")
        }
    }
}

extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
