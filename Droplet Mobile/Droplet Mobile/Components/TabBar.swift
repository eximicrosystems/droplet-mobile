//
//  TabBar.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 29/07/21.
//

import Foundation
import UIKit

class TabBar: UIView {
    
    private var type: Bool!
    @IBOutlet var viewBackground: UIView!
    @IBOutlet var homeIcon: UIImageView!
    @IBOutlet var ordersIcon: UIImageView!
    @IBOutlet var accountIcon: UIImageView!
    @IBOutlet var boxIcon: UIImageView!
    @IBOutlet var homeLabel: UILabel!
    @IBOutlet var ordersLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var boxLabel: UILabel!
    
    @IBOutlet var homeButton: UIButton!
    @IBOutlet var ordersButton: UIButton!
    @IBOutlet var accountButton: UIButton!
    @IBOutlet var yourBoxButton: UIButton!
    
    init(frame: CGRect, type: Bool) {
        self.type = type
        super.init(frame: frame)
        commonInit()
        selectColors()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("TabBar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func selectColors(){
        if self.type {
            viewBackground.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
            homeIcon.image = UIImage(named: "HOME-ICON-WHITE-1")
            ordersIcon.image = UIImage(named: "ORDERS-ICON-WHITE-1")
            accountIcon.image = UIImage(named: "ACCOUNT-ICON-WHITE-1")
            boxIcon.image = UIImage(named: "SHOPPING-BOX-ICON-WHITE-1")
            homeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            ordersLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            accountLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            boxLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else{
            viewBackground.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            homeIcon.image = UIImage(named: "HOME-ICON-1")
            ordersIcon.image = UIImage(named: "ORDERS-ICON-1")
            accountIcon.image = UIImage(named: "ACCOUNT-ICON-1")
            boxIcon.image = UIImage(named: "SHOPPING-BOX-ICON-1")
            homeLabel.textColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
            ordersLabel.textColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
            accountLabel.textColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
            boxLabel.textColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
        }
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(HomeViewController(), animated: false)
        }
    }
    @IBAction func ordersButtonAction(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(NewOrdersViewController(), animated: false)
        }
    }
    @IBAction func accountButtonAction(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(AccountViewController(), animated: false)
        }
    }
    @IBAction func yourBoxButtonAction(_ sender: Any) {
        let yourBox = YourBoxMenu(frame: CGRect(x: 42.0, y: 0.0, width: 382.0, height: 869.0))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(yourBox)
    }
}
