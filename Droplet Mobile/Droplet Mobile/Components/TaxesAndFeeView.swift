//
//  TaxesAndFeeView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 11/08/21.
//

import Foundation
import UIKit

class TaxesAndFeeView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.layer.borderColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("TaxesAndFeeView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    @IBAction func closeButton(_ sender: Any) {
        self.removeFromSuperview()
    }
}
