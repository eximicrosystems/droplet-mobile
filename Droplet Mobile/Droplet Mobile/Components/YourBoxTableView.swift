//
//  YourBoxTableView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 01/10/21.
//

import UIKit
import SwiftUI
import Alamofire
import SwiftyJSON

class YourBoxTableView: UITableView, UITableViewDataSource {
    
    @IBOutlet var table: UITableView!
    private var quantityP: Int = 0
    private var quantityA: Int = 0
    private var nameP: String = ""
    private var nameA: String = ""
    private var priceP: String = "0"
    private var priceA: String = "0"
    
    private var taxes: Float = 6.99
    
    var data: Int = 0
    var showButtons: Bool = true
    
    var yourBox: YourBoxMenu!
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        commonInit()
        getPersistentData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("YourBoxTableView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        
        table.register(YourBoxTableViewCell.nib(), forCellReuseIdentifier: YourBoxTableViewCell.identifier)
        table.dataSource = self
        table.backgroundColor = UIColor.white
        table.separatorColor = UIColor.white
        table.separatorStyle = .none
    }
    
    func getPersistentData(){
        if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
                print(items)
            if let pure = items![1] as? Product{
                    self.quantityP = pure.quantity
                    self.nameP = pure.description
                    self.priceP = "\(pure.cost)"
                }
            if let alkaline = items![0] as? Product{
                self.quantityA = alkaline.quantity
                self.nameA = alkaline.description
                self.priceA = "\(alkaline.cost)"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: YourBoxTableViewCell.identifier,
                                                 for: indexPath) as! YourBoxTableViewCell
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 1000, right: 0)
        cell.yourBox = self.yourBox
        
        if indexPath.row == 0 {
            cell.configure(title: self.nameP, quantity: self.quantityP, price: self.priceP, type: "pure",showButton: showButtons)
        }else{
            cell.configure(title: self.nameA, quantity: self.quantityA, price: self.priceA, type: "alkaline", showButton: showButtons)
        }
        cell.delegate = self
        return cell
    }
}

extension YourBoxTableView: MyTableViewCellDelegate{
    func didTabButton() {
        yourBox.getPersistentData()
        print("Recalculando cuenta")
    }
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
