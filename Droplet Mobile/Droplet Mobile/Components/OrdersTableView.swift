//
//  OrdersTableView.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 08/11/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class OrdersTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
        
    @IBOutlet var table: UITableView!
    var ordersStatus:[OrderStatus]!
    var newOrders: NewOrdersViewController!
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("OrdersTableView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        table.register(OrdersTableViewCell.nib(), forCellReuseIdentifier: OrdersTableViewCell.identifier)
        table.dataSource = self
        table.delegate = self
        table.backgroundColor = UIColor.white
    }
   
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersStatus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrdersTableViewCell.identifier,
                                                 for: indexPath) as! OrdersTableViewCell
        cell.configure(date: ordersStatus[indexPath.row].date, status: ordersStatus[indexPath.row].status, amount: ordersStatus[indexPath.row].amount)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Se seleccionó la celda: \(indexPath.row)")
        newOrders.addView(order: ordersStatus[indexPath.row])
//        let cell = tableView.cellForRow(at: indexPath) as? OrdersTableViewCell
//        print("la fecha es: \(cell?.date.text!), tiene el estatus: \(cell?.status.text!) y fue por la cantidad \(cell?.amount.text!)")
//        cell?.selectRow()
        
    }
}
