//
//  YourBoxMenu.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 29/07/21.
//

import Foundation
import UIKit
import Popover
import DropDown
import BraintreeDropIn
import Braintree
import SVProgressHUD
import FirebaseCrashlytics
import Alamofire
import SwiftyJSON

class YourBoxMenu: UIView {
    
    @IBOutlet var drinkButton: UIButton!
    @IBOutlet var moreAddressButton: UIButton!
    @IBOutlet var speedButton: UIButton!
    @IBOutlet var tableView: UIView!
    @IBOutlet var subtotalData: UIView!
    @IBOutlet var dropdownPercent: UIView!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var percent: UILabel!
    @IBOutlet var deliverySpeed: UILabel!
    @IBOutlet var deliveryCost: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var taxesfee: UILabel!
    @IBOutlet var deliveryfee: UILabel!
    @IBOutlet var tipPercentage: UILabel!
    @IBOutlet var paymentOptions: UIView!
    @IBOutlet var paymentMethod1: UILabel!
    @IBOutlet var addPaymentMethod: UIView!
    @IBOutlet var addCardView: UIView!
    @IBOutlet var saveInfo: UIButton!
    
    private var quantityP: Int = 0
    private var quantityA: Int = 0
    private var priceP: String = "0"
    private var priceA: String = "0"
    private var taxes: Float = 6.99
    
    var statusTransaction: Bool = false
    let dropDown = DropDown()
    let percentData = ["5%","10%", "15%", "20%"]
    let toKinizationKey = "sandbox_8hj9x3n7_h4wcxj3dthb855m6"
    
    private var user: User!
    private var box: Box!
    
    private var paymentRequest : PKPaymentRequest = {
        let request = PKPaymentRequest()
        request.merchantIdentifier = "merchant.avotreservice.us"
        request.supportedNetworks = [.quicPay, .masterCard, .visa, .discover]
        request.supportedCountries = ["US"]
        request.merchantCapabilities = .capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Droplet shipping", amount: 44.94)]
        return request
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        addDropdown()
        showTable()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("YourBoxMenu", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        if let address = UserDefaults.standard.value(forKey: "address") as? String{
            self.addressLbl.text = address
        }
    }
    
    func addDropdown(){
        dropDown.anchorView = dropdownPercent
        dropDown.dataSource = percentData
        // Top of drop down will be below the anchorView
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            percent.text = item
        }
    }
    
    func showTable(){
        var data = 0
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: products)
            if let pure = items![1] as? Product{
                if pure.quantity > 0 {
                    data += 1
                }
            }
            if let alkaline = items![0] as? Product{
                if alkaline.quantity > 0 {
                    data += 1
                }
            }
        }
        
        if data>0{
            let table = YourBoxTableView(frame: CGRect(x: 0, y: 0, width: 333, height:100), style: .plain)
            table.data = data
            table.yourBox = self
            table.showButtons = false
            table.separatorColor = UIColor.white
            table.separatorStyle = .none
            tableView.addSubview(table)
            getPersistentData()
            drinkButton.isEnabled = true
        }else{
            subtotalData.frame.origin.y -= 100
            drinkButton.isEnabled = false
        }
    }
    
    @IBAction func showPercent(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func payView(_ sender: Any) {
        self.removeFromSuperview()
        if paymentMethod1.text == "APPLE PAY"{
            applePayMethod()
        }else {
            showDropIn()
        }
    }
    
    func getPersistentData(){
        if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
                //print(items)
            if let pure = items![1] as? Product{
                    self.quantityP = pure.quantity
                    self.priceP = "\(pure.cost)"
                }
            if let alkaline = items![0] as? Product{
                self.quantityA = alkaline.quantity
                self.priceA = "\(alkaline.cost)"
            }
        }
        updateTotal()
    }
    
    func updateTotal(){
        let delivery = self.deliveryfee.text ?? "0"
        let total = taxes + (Float(self.quantityP)*priceP.floatValue) + (Float(self.quantityA)*priceA.floatValue)
        self.amount.text = String(format: "%.2f", total+delivery.floatValue)
    }

    func prepareOrder() -> Order{
        let defaults = UserDefaults.standard
        if let data = defaults.value(forKey: "user") as? Data {
            var currentUser = try? PropertyListDecoder().decode(User.self, from: data)
            let shippingAddress = addressLbl.text?.components(separatedBy: ",")
            if shippingAddress!.count == 4 {
                currentUser?.shipping.address_1 = shippingAddress?[0]
                currentUser?.shipping.city = shippingAddress?[1]
                currentUser?.shipping.state = shippingAddress?[2]
                currentUser?.shipping.postcode = shippingAddress?[3]
                currentUser?.shipping.country = "US"
                
                currentUser?.billing.address_1 = shippingAddress?[0]
                currentUser?.billing.city = shippingAddress?[1]
                currentUser?.billing.state = shippingAddress?[2]
                currentUser?.billing.postcode = shippingAddress?[3]
                currentUser?.billing.country = "US"
                user = currentUser
                defaults.set(try? PropertyListEncoder().encode(currentUser), forKey: "user")
                defaults.synchronize()
            }
        }
        
        if let products = defaults.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: products)
            box = Box(items: [
                Items(product_id: items![0].product_id, quantity: items![0].quantity),
                Items(product_id: items![1].product_id, quantity: items![1].quantity)
            ], taxes_fee: Float(taxesfee.text!)!,
            delivery_fee: Float(deliveryfee.text!)!,
            tip_percentage: Float(tipPercentage.text!)!,
            total: Float(amount.text!)!)
            defaults.set(try? PropertyListEncoder().encode(box), forKey: "box")
            defaults.synchronize()
        }
        
        return Order(payment_method: "bacs", payment_method_title: "Direct Bank Transfer", set_paid: true, billing: user.billing, shipping: user.shipping, line_items: box.items, shipping_lines: [
            ShippingType(method_id: "flat_rate", method_title: "Flat Rate", total: String(format: "%.2f", box.total))
        ])
    }
    
    func showDropIn(){
        let request =  BTDropInRequest()
        request.venmoRequest?.profileID = "1953896702662410263"
        request.venmoDisabled = false
        request.vaultManager = true
        let dropIn = BTDropInController(authorization: toKinizationKey, request: request) { (controller, result, error) in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            } else if (result?.isCanceled == true) {
                SVProgressHUD.showInfo(withStatus: "Transaction Cancelled")
                controller.dismiss(animated: true, completion: nil)
            } else if let nonce = result?.paymentMethod?.nonce, let amount = self.amount.text {
                print("Total amont: \(amount)")
                print("El estatus de pago fue: \(self.sendRequestPaymentToServer(nonce: nonce, amount: amount))")
                
                controller.dismiss(animated: true, completion: {
                    WooCommerce.shared.createOrder(order: self.prepareOrder()) { response in
                        if let status = response, status == "processing" {
                            if let topVC = UIApplication.getTopViewController() {
                                    topVC.navigationController?.pushViewController(ConfirmedViewController(), animated: false)
                            }
                        }else{
                            SVProgressHUD.showError(withStatus: "No se creo la orden")
                        }
                    }
                })
            }
        }
        UIApplication.shared.keyWindow!.rootViewController?.present(dropIn!, animated: true, completion: nil)
    }
    
    func applePayMethod(){
        let controller = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
        if controller != nil{
            controller!.delegate = self
            UIApplication.shared.keyWindow!.rootViewController?.present(controller!, animated: true, completion: nil)
        }
    }
    
    func sendRequestPaymentToServer(nonce: String, amount: String) -> Bool{
        SVProgressHUD.show()
        let paymentURL = URL(string: "http://192.168.64.2/donate/pay.php")!
        var request = URLRequest(url: paymentURL)
        request.httpBody = "payment_method_nonce=\(nonce)&amount=\(amount)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            guard let data = data else {
                SVProgressHUD.showError(withStatus: error!.localizedDescription)
                return
            }
            guard let result = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let success = result["success"] as? Bool, success == true else {
                SVProgressHUD.showSuccess(withStatus: "Successfully charged.")
                //SVProgressHUD.showError(withStatus: "Transaction failed. Please try again.")
                return
            }
            SVProgressHUD.showSuccess(withStatus: "Successfully charged. Thanks So Much :)")
        }.resume()
        return self.statusTransaction
    }
    
    @IBAction func changeAddress(_ sender: Any) {
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:220))
        let addressView = AddAddressView(frame: CGRect(x: 0, y: 10, width: aView.frame.width, height: 200))
        addressView.yourBox = self
        let popoverView = Popover(options: nil, showHandler: nil, dismissHandler: nil)
        addressView.popUp = popoverView
        aView.addSubview(addressView)
        popoverView.show(aView, fromView: self.moreAddressButton)
    }
    
    @IBAction func speedButton(_ sender: Any){
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: 287, height:185))
        let delivery = DeliverySpeedView(frame: CGRect(x: 0, y: 20, width: aView.frame.width, height: 175))
        let popoverView = Popover(options: nil, showHandler: nil, dismissHandler: nil)
        delivery.setBox(box: self)
        delivery.setPop(pop: popoverView)
        aView.addSubview(delivery)
        popoverView.show(aView, fromView: self.speedButton)
    }
    
    @IBAction func infoButton(_ sender: Any) {
        let tfView = TaxesAndFeeView(frame: CGRect(x: 20, y: 335, width: UIScreen.main.bounds.width-78, height:300))
        self.addSubview(tfView)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.isHidden = true
    }
    
    @IBAction func paymentOptions(_ sender: Any) {
        paymentOptions.isHidden = false
    }
    @IBAction func closePaymentOptions(_ sender: Any) {
        paymentOptions.isHidden = true
    }
    @IBAction func applePaybutton(_ sender: Any) {
        paymentMethod1.text = "APPLE PAY"
        paymentOptions.isHidden = true
    }
    @IBAction func venmoPayButton(_ sender: Any) {
        paymentMethod1.text = "VENMO"
        paymentOptions.isHidden = true
    }
    @IBAction func visaPayButton(_ sender: Any) {
        paymentMethod1.text = "Credit Card ****0123"
        paymentOptions.isHidden = true
    }
    @IBAction func discoverPayButton(_ sender: Any) {
        paymentMethod1.text = "Credit Card ****9999"
        paymentOptions.isHidden = true
    }
    @IBAction func addPaymentMethodButton(_ sender: Any) {
        paymentOptions.isHidden = true
        addPaymentMethod.isHidden = false
        saveInfo.layer.borderWidth = 2
        saveInfo.layer.borderColor = #colorLiteral(red: 0.04274367541, green: 0.302154094, blue: 0.5413638949, alpha: 1)
    }
    @IBAction func backButton(_ sender: Any) {
        paymentOptions.isHidden = true
        addPaymentMethod.isHidden = true
    }
    @IBAction func addCreditCardButton(_ sender: Any) {
        addCardView.isHidden = false
    }
}

extension YourBoxMenu : PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
    }
}
