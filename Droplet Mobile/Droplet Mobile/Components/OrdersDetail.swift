//
//  OrdersDetail.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 13/11/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class OrdersDetail: UIView {
    
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var OrdersDetailView: UIView!
    @IBOutlet var taxesfeeLabel: UILabel!
    @IBOutlet var deliveryLabel: UILabel!
    @IBOutlet var percentLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    
    var order: OrderStatus!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        components()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("OrdersDetail", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func components(){
        OrdersDetailView.layer.borderColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
        OrdersDetailView.layer.borderWidth = 3
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            self.searhOrder()
            self.removeFromSuperview()
        })
    }
    
    func searhOrder(){
        DispatchQueue.main.async {
            let user = "ck_3a2787bf6160932511414e21f6f3d564f9e8d155"
            let password = "cs_2da22a81fd1c2bc8f91523e5d69757d9d4fb01da"
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/orders?consumer_key=\(user)&consumer_secret=\(password)")
                .authenticate(username: user, password: password)
                .responseJSON { response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        for data in json{
                            if let orderStatus = data.1["id"].int, orderStatus == self.order.id {
                                print(data.1)
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
        }
    }
}
