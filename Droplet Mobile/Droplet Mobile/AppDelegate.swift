//
//  AppDelegate.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 21/07/21.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseAuth
import GooglePlaces
//import Braintree.BTAppContextSwitcher

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        FirebaseApp.configure()
        GMSPlacesClient.provideAPIKey("AIzaSyBUkeRP0p7tfYAzJfASht_RZHPu4yiOoc8")
//        BTAppSwitch.setReturnURLScheme("com.droplet-company.droplet.payments")
        if #available(iOS 10, *){
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]){
                (granted, error) in }
            application.registerForRemoteNotifications()
        }else{
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

