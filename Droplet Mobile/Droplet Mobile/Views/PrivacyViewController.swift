//
//  PrivacyViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 24/09/21.
//

import UIKit

class PrivacyViewController: UIViewController {

    @IBOutlet var textInfo: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addComponents()
    }
    
    func addComponents() {
        textInfo.backgroundColor = UIColor.white
        let navBar = NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: "")
        let tabBar = TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false)
        self.view.addSubview(navBar)
        self.view.addSubview(tabBar)
    }
}
