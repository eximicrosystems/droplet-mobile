//
//  MenuViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 22/07/21.
//

import Foundation
import UIKit
import Popover
import SVProgressHUD

class MenuViewController: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }

    func addComponents() {
        let navBar = NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: nil)
        let tabBar = TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: true)
        self.view.addSubview(navBar)
        self.view.addSubview(tabBar)
    }
    
    @IBAction func hidrationButton(_ sender: Any) {
        self.navigationController?.pushViewController(HidrationViewController(), animated: false)
    }
    @IBAction func immunityButton(_ sender: Any) {
        SVProgressHUD.show()
        self.navigationController?.pushViewController(ImmunityViewController(), animated: false)
    }
    @IBAction func antiButton(_ sender: Any) {
        SVProgressHUD.show()
        self.navigationController?.pushViewController(AntiViewController(), animated: false)
    }
}
