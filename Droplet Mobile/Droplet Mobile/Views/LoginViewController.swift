//
//  LoginViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 20/09/21.
//

import UIKit
import SVProgressHUD
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var codeNumber: UITextField!
    @IBOutlet var sendCodeButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var enterMobileMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if let verificationID = UserDefaults.standard.string(forKey: "authVerificationID"), let verificationCode = codeNumber.text{
            let credential = PhoneAuthProvider.provider().credential(
              withVerificationID: verificationID,
              verificationCode: verificationCode
            )
            Auth.auth().signIn(with: credential){ (user, error) in
                if error != nil{
                    print("error: \(String(describing: error?.localizedDescription))")
                }else{                    
                    SVProgressHUD.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                        SVProgressHUD.dismiss()
                        self.navigationController?.pushViewController(HomeViewController(), animated: false)
                    })
                }
            }
        }
    }
    
    @IBAction func sendCodeButtonAction(_ sender: Any) {
            if let phoneNumber = phoneNumber.text {
                PhoneAuthProvider.provider()
                  .verifyPhoneNumber("+52"+phoneNumber, uiDelegate: nil) { verificationID, error in
                      if let error = error {
                        print("error: \(error.localizedDescription)")
                        return
                      }else{
                        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                      }
                  }
                enterMobileMessage.text = "INSERT YOUR CODE"
                self.phoneNumber.isHidden = true
                codeNumber.isHidden = false
                loginButton.isHidden = false
                sendCodeButton.isHidden = true
            }
    }
}
