//
//  ConfirmedViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 22/07/21.
//

import Foundation
import UIKit

class ConfirmedViewController: UIViewController {
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var tableView: UIView!
    @IBOutlet var deliverTo: UILabel!
    @IBOutlet var taxesFee: UILabel!
    @IBOutlet var deliveryFee: UILabel!
    @IBOutlet var tipPercentage: UILabel!
    @IBOutlet var total: UILabel!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        showTablev2()
    }
    
    func addComponents() {
        let navBar = NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: nil)
        let tabBar = TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false)
        self.view.addSubview(navBar)
        self.view.addSubview(tabBar)
    }
    
    func showTable(){
        var data = 0
        let userDefaults = UserDefaults.standard
        if let ud = userDefaults.value(forKey: "pure"){
            print(ud)
            data += 1
        }
        if let ud = userDefaults.value(forKey: "alkaline"){
            print(ud)
            data += 1
        }
        if data>0{
            let table = YourBoxTableView(frame: CGRect(x: 0, y: 0, width: 291, height:100), style: .plain)
            table.data = data
            table.showButtons = true
            table.separatorColor = UIColor.white
            table.separatorStyle = .none
            tableView.addSubview(table)
        }
    }
    func showTablev2(){
        var data = 0
        if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
            data = items?.count ?? 0
        }
        if data>0{
            let table = YourBoxTableView(frame: CGRect(x: 0, y: 0, width: 291, height:100), style: .plain)
            table.data = data
            table.showButtons = true
            table.separatorColor = UIColor.white
            table.separatorStyle = .none
            tableView.addSubview(table)
        }
    }
    
    func showConfirmed(){
        let defaults = UserDefaults.standard
        if let data = defaults.value(forKey: "box") as? Data {
            let box = try? PropertyListDecoder().decode(Box.self, from: data)
            self.deliverTo.text = box!.deliver_to
            self.taxesFee.text = String(format: "%.2f", box!.taxes_fee)
            self.deliveryFee.text = String(format: "%.2f", box!.delivery_fee)
            self.tipPercentage.text = String(format: "%.2f", box!.tip_percentage)
            self.total.text = String(format: "%.2f", box!.total)
        }
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "box")
        defaults.removeObject(forKey: "items")
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(HomeViewController(), animated: false)
        }
    }
    
    @IBAction func closeButtonTemp(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "box")
        defaults.removeObject(forKey: "items")
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(HomeViewController(), animated: false)
        }
    }
    
    
}
