//
//  NotDeliveryViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 21/07/21.
//

import Foundation
import UIKit

class NotDeliveryViewController: UIViewController {
    
    @IBOutlet var messageView: UIView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageView.layer.borderColor = #colorLiteral(red: 0.2549019608, green: 0.4509803922, blue: 0.1607843137, alpha: 1)
        messageView.layer.borderWidth = 3
        
    }
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.pushViewController(HomeViewController(), animated: false)
    }
}
