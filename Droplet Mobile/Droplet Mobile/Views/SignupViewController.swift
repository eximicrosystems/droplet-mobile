//
//  SignupViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 20/09/21.
//

import UIKit
import FirebaseFirestore
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SignupViewController: UIViewController {

    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mobileNumber: UITextField!
    
    private let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signup(_ sender: Any) {
        checkUserOnFirebase()
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func checkUserOnFirebase(){
        print("Checking User")
        if let mobilePhone = self.mobileNumber.text, mobilePhone.count > 0 {
            db.collection("users").document(mobilePhone).getDocument{(documentSnapshot, error) in
                if let document = documentSnapshot, error == nil{
                    if let phone = document.get("phone") as? String{
                        print("Si existe el usuario: \(phone)")
                        self.navigationController?.pushViewController(LoginViewController(), animated: false)
                    }
                }
                return
            }
            if let firstName = self.firstName.text, let lastName = self.lastName.text, firstName.count>0, lastName.count>0 {
                self.createUser(firstName: firstName, lastName: lastName, phoneNumber: mobilePhone)
            }else{
                print("Faltan datos para el registro")
            }
        }else{
            print("Ingresa número de teléfono")
        }
    }
    
    func createUser(firstName: String, lastName: String, phoneNumber: String){
        SVProgressHUD.show()
        print("Add User")
        //Create obj User
        var userWC = User(email: "\(firstName)_\(lastName)@droplet.com",
                          first_name: firstName.uppercased(),
                          last_name: lastName.uppercased(),
                          billing: Billing(first_name: firstName, last_name: lastName, email: "\(firstName)_\(lastName)@droplet.com", phone: phoneNumber),
                          shipping: Shipping(first_name: firstName, last_name: lastName)
                    )
        //Add User on WooCommerce
        WooCommerce.shared.createUser(userWooCommerce: userWC){ response in
            if let id = response {
                //add id to user
                userWC.id = id
                userWC.username = "\(firstName)_\(lastName)"
                //Add User on Firebase
                self.db.collection("users").document(phoneNumber).setData(
                    ["idWooCommerce": id,
                     "first_name": userWC.first_name,
                     "last_name": userWC.last_name,
                     "phone": phoneNumber])
                
                let defaults = UserDefaults.standard
                defaults.set(try? PropertyListEncoder().encode(userWC), forKey: "user")
                defaults.synchronize()
                
                SVProgressHUD.dismiss()
                self.navigationController?.pushViewController(HomeViewController(), animated: false)
            }
        }
    }
}
