//
//  WellnessViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 19/09/21.
//

import UIKit

class WellnessViewController: UIViewController {

    var lateralMenu1: LateralMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lateralMenu1 = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
    }
    
    @IBAction func beveragesButton(_ sender: Any) {
        self.navigationController?.pushViewController(NotDeliveryViewController(), animated: false)
    }
    
    
    @IBAction func supplementsButton(_ sender: Any) {
        self.navigationController?.pushViewController(NotDeliveryViewController(), animated: false)
    }
    @IBAction func menuButton(_ sender: Any) {
        UIView.transition(with: self.view, duration: 0.25, options: .transitionCrossDissolve, animations: {
                self.view.addSubview(self.lateralMenu1)
                self.view.bringSubviewToFront(self.lateralMenu1)
            }, completion: nil)
    }
}
