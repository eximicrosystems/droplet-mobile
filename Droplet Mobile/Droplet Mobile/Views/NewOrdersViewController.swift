//
//  NewOrdersViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 29/07/21.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class NewOrdersViewController: UIViewController {

    @IBOutlet var tableView: UIView!
    private var ordersStatus = [OrderStatus]()
    private var user: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        getUser()
        sendRequest()
    }
    
    func addComponents() {
        self.view.addSubview(NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: "orders"))
        self.view.addSubview(TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false))
    }
    
    func getUser(){
        if let data = UserDefaults.standard.value(forKey: "user") as? Data {
            let currentUser = try? PropertyListDecoder().decode(User.self, from: data)
            self.user = currentUser?.first_name as? String
        }
    }
    
    func sendRequest(){
        DispatchQueue.main.async {
            let user = "ck_3a2787bf6160932511414e21f6f3d564f9e8d155"
            let password = "cs_2da22a81fd1c2bc8f91523e5d69757d9d4fb01da"
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/orders?consumer_key=\(user)&consumer_secret=\(password)")
                .authenticate(username: user, password: password)
                .responseJSON { response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        for data in json{
                            if let orderName = data.1["shipping"]["first_name"].string?.uppercased(), orderName == self.user {
                                print(data.1)
                                self.ordersStatus.append(OrderStatus(id: data.1["id"].int!,
                                                                date: data.1["date_created"].string!,
                                                                status: data.1["status"].string!,
                                                                amount: data.1["total"].string!))
                            }
                        }
                        print("Se encontraron: \(self.ordersStatus.count) registros")
                        self.showTable()
                    case .failure(let error):
                        print(error)
                    }
                }
        }
    }
    
    func showTable(){
        if ordersStatus.count > 0{
            tableView.isHidden = false
            let table = OrdersTableView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height), style: .plain)
            table.ordersStatus = self.ordersStatus
            table.newOrders = self
            table.separatorColor = UIColor.white
            table.separatorStyle = .none
            tableView.addSubview(table)
        }
    }
    
    func addView(order: OrderStatus){
        let orderView = OrdersDetail()
        orderView.order = order
        self.view.addSubview(orderView)
    }
}
