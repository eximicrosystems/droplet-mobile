//
//  HomeViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 21/07/21.
//

import Foundation
import UIKit
import FirebaseFirestore
import FirebaseCrashlytics

class HomeViewController: UIViewController {
    
    private let db = Firestore.firestore()
    var lateralMenu1: LateralMenu!
    @IBOutlet var welcome: UILabel!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let data = UserDefaults.standard.value(forKey: "user") as? Data {
            let currentUser = try? PropertyListDecoder().decode(User.self, from: data)
            self.welcome.text = "HELLO \(currentUser?.first_name ?? "")"
        }
    }
    
    @IBAction func watrButton(_ sender: Any) {
        self.navigationController?.pushViewController(MenuViewController(), animated: false)
    }
    
    @IBAction func naturalButton(_ sender: Any) {
        self.navigationController?.pushViewController(WellnessViewController(), animated: false)
    }
    
    @IBAction func menuButton(_ sender: Any) {
        lateralMenu1 = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIView.transition(with: self.view, duration: 0.25, options: .transitionCrossDissolve, animations: {
                self.view.addSubview(self.lateralMenu1)
                self.view.bringSubviewToFront(self.lateralMenu1)
            }, completion: nil)
    }
    
}
