//
//  AccountViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 29/07/21.
//

import Foundation
import UIKit
import FirebaseFirestore
import SVProgressHUD

class AccountViewController: UIViewController {
    
    @IBOutlet var accountInfoButton: UIButton!
    @IBOutlet var accountAddress: UIButton!
    @IBOutlet var paymentInfoButton: UIButton!
    @IBOutlet var supportButton: UIButton!
    @IBOutlet var accountInfoView: UIView!
    @IBOutlet var accountAddressView: UIView!
    @IBOutlet var paymentInfoView: UIView!
    @IBOutlet var supportView: UIView!
    @IBOutlet var dropdownac: UIImageView!
    @IBOutlet var dropdown: UIImageView!
    @IBOutlet var dropdownpi: UIImageView!
    @IBOutlet var dropdowns: UIImageView!
    @IBOutlet var checkmarkImage: UIImageView!
    @IBOutlet var saveAddressButton: UIButton!
    @IBOutlet var saveInfoButton: UIButton!
    
    //Info data
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mobilePhone: UITextField!
    @IBOutlet var emal: UITextField!
    //Addresses data
    @IBOutlet var deliveryStreet1: UITextField!
    @IBOutlet var deliveryStreet2: UITextField!
    @IBOutlet var deliveryCity: UITextField!
    @IBOutlet var deliveryZip: UITextField!
    @IBOutlet var deliveryState: UITextField!
    @IBOutlet var billingStreet1: UITextField!
    @IBOutlet var billingStreet2: UITextField!
    @IBOutlet var billingCity: UITextField!
    @IBOutlet var billingZip: UITextField!
    @IBOutlet var billingState: UITextField!
    
    private let db = Firestore.firestore()
    
    var flagMenu = false
    var lateralMenu1: LateralMenu!
    var flagBox = false
    var boxMenu: YourBoxMenu!
    var flagai = false
    var flagaa = false
    var flagpi = false
    var flags = false
    var isChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lateralMenu1 = LateralMenu(frame: CGRect(x: 80.0, y: 84.0, width: 334.0, height: 852.0))
        boxMenu = YourBoxMenu(frame: CGRect(x: 42.0, y: 0.0, width: 382.0, height: 869.0))
        saveAddressButton.layer.borderColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
        saveInfoButton.layer.borderColor = #colorLiteral(red: 0.1019607843, green: 0.231372549, blue: 0.4509803922, alpha: 1)
        addComponents()
//        addInfo()
        readInfo()
    }
    
    func addComponents() {
        let navBar = NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: "account")
        let tabBar = TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false)
        self.view.addSubview(navBar)
        self.view.addSubview(tabBar)
    }
    
    func addInfo(){
        db.collection("users").document("5560642384").getDocument{(documentSnapshot, error) in
            if let document = documentSnapshot, error == nil{
                if let name = document.get("firstName") as? String{
                    self.firstName.text = name.uppercased()
                }
                if let lastName = document.get("lastName") as? String{
                    self.lastName.text = lastName.uppercased()
                }
                if let phone = document.get("phone") as? String{
                    self.mobilePhone.text = phone
                }
                if let email = document.get("email") as? String{
                    self.emal.text = email.uppercased()
                }
            }
        }
    }
    
    func readInfo(){
        let defaults = UserDefaults.standard
        if let data = defaults.value(forKey: "user") as? Data {
            let currentUser = try? PropertyListDecoder().decode(User.self, from: data)
            firstName.text = currentUser?.first_name
            lastName.text = currentUser?.last_name
            mobilePhone.text = currentUser?.billing.phone
            emal.text = currentUser?.billing.email
            
            deliveryStreet1.text = currentUser?.shipping.address_1
            deliveryStreet2.text = currentUser?.shipping.address_2
            deliveryCity.text = currentUser?.shipping.city
            deliveryState.text = currentUser?.shipping.state
            deliveryZip.text = currentUser?.shipping.postcode
            
            billingStreet1.text = currentUser?.billing.address_1
            billingStreet2.text = currentUser?.billing.address_2
            billingCity.text = currentUser?.billing.city
            billingState.text = currentUser?.billing.state
            billingZip.text = currentUser?.billing.postcode
        }
    }
    
    func saveAccountInfo(){
        SVProgressHUD.show()
        let defaults = UserDefaults.standard
        if var currentUser = try? PropertyListDecoder().decode(User.self, from: (defaults.value(forKey: "user") as? Data)!){
            currentUser.first_name = firstName.text!
            currentUser.last_name = lastName.text!
            currentUser.billing.phone = mobilePhone.text!
            currentUser.billing.email = emal.text!
            defaults.set(try? PropertyListEncoder().encode(currentUser), forKey: "user")
        }
        defaults.synchronize()
        SVProgressHUD.dismiss()
    }
    
    func saveAccountAddress(){
        SVProgressHUD.show()
        let defaults = UserDefaults.standard
        if var currentUser = try? PropertyListDecoder().decode(User.self, from: (defaults.value(forKey: "user") as? Data)!){
            currentUser.shipping.address_1 = billingStreet1.text
            currentUser.shipping.address_2 = billingStreet2.text
            currentUser.shipping.city = billingCity.text
            currentUser.shipping.state = billingState.text
            currentUser.shipping.postcode = billingZip.text
            
            if isChecked {
                currentUser.billing.address_1 = billingStreet1.text
                currentUser.billing.address_2 = billingStreet2.text
                currentUser.billing.city = billingCity.text
                currentUser.billing.state = billingState.text
                currentUser.billing.postcode = billingZip.text
            }else {
                currentUser.billing.address_1 = deliveryStreet1.text
                currentUser.billing.address_2 = deliveryStreet2.text
                currentUser.billing.city = deliveryCity.text
                currentUser.billing.state = deliveryState.text
                currentUser.billing.postcode = deliveryZip.text
            }
            defaults.set(try? PropertyListEncoder().encode(currentUser), forKey: "user")
        }
        defaults.synchronize()
        SVProgressHUD.dismiss()
    }
    
    @IBAction func saveInfo(_ sender: Any) {
//        SVProgressHUD.show()
//        self.db.collection("users").document("5560642384").setData(
//            ["firstName":self.firstName.text?.uppercased() ?? "",
//             "lastName": self.lastName.text?.uppercased() ?? "",
//             "phone": self.mobilePhone.text ?? "",
//             "email": self.emal.text ?? ""])
//        SVProgressHUD.dismiss()
        saveAccountInfo()
    }
    
    @IBAction func saveAddress(_ sender: Any) {
        saveAccountAddress()
    }
    
    @IBAction func accountInfoAction(_ sender: Any) {
        if flagai {
            accountInfoView.isHidden = true
            accountAddress.frame.origin.y -= 100
            paymentInfoButton.frame.origin.y -= 100
            supportButton.frame.origin.y -= 100
            dropdown.frame.origin.y -= 100
            dropdownpi.frame.origin.y -= 100
            dropdowns.frame.origin.y -= 100
            flagai = false
        }else{
            accountInfoView.isHidden = false
            accountAddress.frame.origin.y += 100
            paymentInfoButton.frame.origin.y += 100
            supportButton.frame.origin.y += 100
            dropdown.frame.origin.y += 100
            dropdownpi.frame.origin.y += 100
            dropdowns.frame.origin.y += 100
            flagai = true
        }
    }
    @IBAction func accountAddressAction(_ sender: Any) {
        if flagaa {
            accountAddressView.isHidden = true
            paymentInfoButton.frame.origin.y -= 290
            supportButton.frame.origin.y -= 290
            dropdownpi.frame.origin.y -= 290
            dropdowns.frame.origin.y -= 290
            flagaa = false
        }else{
            accountAddressView.isHidden = false
            paymentInfoButton.frame.origin.y += 290
            supportButton.frame.origin.y += 290
            dropdownpi.frame.origin.y += 290
            dropdowns.frame.origin.y += 290
            flagaa = true
        }
    }
    @IBAction func paymentInfoAction(_ sender: Any) {
        if flagpi {
            paymentInfoView.isHidden = true
            supportButton.frame.origin.y -= 180
            dropdowns.frame.origin.y -= 180
            flagpi = false
        }else{
            paymentInfoView.isHidden = false
            supportButton.frame.origin.y += 180
            dropdowns.frame.origin.y += 180
            flagpi = true
        }
    }
    @IBAction func supportAction(_ sender: Any) {
        if flags {
            supportView.isHidden = true
            flags = false
        }else{
            supportView.isHidden = false
            flags = true
        }
    }
    
    @IBAction func checkmarkAction(_ sender: Any) {
        if isChecked {
            checkmarkImage.image = UIImage(systemName: "square")
            isChecked = false
        }else{
            checkmarkImage.image = UIImage(systemName: "checkmark.square")
            isChecked = true
        }
    }
    
}
