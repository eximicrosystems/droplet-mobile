//
//  HidrationViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 22/07/21.
//

import Foundation
import UIKit
import Popover
import Alamofire
import SwiftyJSON
import SVProgressHUD

enum Types: String {
    case water
    case alkaline
}

class HidrationViewController: UIViewController {
    
    @IBOutlet var title1: UILabel!
    @IBOutlet var infoButton1: UIButton!
    @IBOutlet var product1: UILabel!
    
    @IBOutlet var title2: UILabel!
    @IBOutlet var infoButton2: UIButton!
    @IBOutlet var product2: UILabel!
    
    private var products = [Product]()
    private var items = [Items]()
    
    var flagMenu = false
    var flagBox = false
    var pureQuantity = 0
    var alkalineQuantity = 0
    var boxMenu: YourBoxMenu!

    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        sendRequest()
    }
    
    func addComponents() {
        title1.text = ""
        title2.text = ""
        product1.text = ""
        product2.text = ""
        self.view.addSubview(NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: nil))
        self.view.addSubview(TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false))
    }
    //https://www.droplet.delivery/wp-json/wc/v3/products?consumer_key=ck_3a2787bf6160932511414e21f6f3d564f9e8d155&consumer_secret=cs_2da22a81fd1c2bc8f91523e5d69757d9d4fb01da
    func sendRequest() {
        DispatchQueue.main.async {
            let user = "ck_90255c5acbcb24a331615bfbdf49e861c1a4c6e2"
            let password = "cs_9df9c8eaee91abf9fd8eb5a45c7dd0798cca6770"
//            let user = "droplet_r8p86m"
//            let password = "2rBhtG5NOEjUwZHI"
            AF.request("https://www.droplet.delivery/wp-json/wc/v3/products?consumer_key=\(user)&consumer_secret=\(password)")
                .authenticate(username: user, password: password)
                .responseJSON { response in
                    switch response.result{
                    case .success(let value):
                        let json = JSON(value)
                        for data in json{
                            if let id = data.1["id"].int{
                                if id == 134 {
                                    if let name = data.1["name"].string, let cost = data.1["price"].string, let stock = data.1["stock_status"].string {
                                        self.products.append(Product(product_id: 134,
                                                                     name: name,
                                                                     description: "ALKALINE + ELECTROLYTES (12 BOTTLES)",
                                                                     cost: Int(cost)!,
                                                                     quantity: 0,
                                                                     stock_status: stock))
                                        self.title2.text = name
                                        self.product2.text = "$\(cost) + DELIVERY"
                                    }
                                }else if id == 127 {
                                    if let name = data.1["name"].string, let cost = data.1["price"].string, let stock = data.1["stock_status"].string {
                                        self.products.append(Product(product_id: 127,
                                                                     name: name,
                                                                     description: "PURE + ELECTROLYTES (12 BOTTLES)",
                                                                     cost: Int(cost)!,
                                                                     quantity: 0,
                                                                     stock_status: stock))
                                        self.title1.text = name
                                        self.product1.text = "$\(cost) + DELIVERY"
                                    }
                                }
                            }
                        }
                        self.getValues()
                        let defaults = UserDefaults.standard
                        defaults.set(try? PropertyListEncoder().encode(self.products), forKey: "items")
                        defaults.synchronize()
                        
                    case .failure(let error):
                        print(error)
                    }
                }
        }
    }
    
    func getValues(){
        if let pruducts = UserDefaults.standard.value(forKey: "items") as? Data {
            let items = try? PropertyListDecoder().decode(Array<Product>.self, from: pruducts)
            for data in items! {
                if data.product_id == 127 {
                    var pure = self.products[1] as? Product
                    pure?.quantity = data.quantity
                    self.products[1] = pure!
                }else{
                    var alkaline = self.products[0] as? Product
                    alkaline?.quantity = data.quantity
                    self.products[0] = alkaline!
                }
            }
        }
    }
    
    @IBAction func drinkButton(_ sender: Any) {
        getValues()
        if var pure = self.products[1] as? Product{
            let defaults = UserDefaults.standard
            if pure.stock_status == "instock"{
                pure.quantity += 1
                self.products[1] = pure
                defaults.set(try? PropertyListEncoder().encode(products), forKey: "items")
                defaults.synchronize()
                UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(YourBoxMenu(frame: CGRect(x: 42.0, y: 0.0, width: 382.0, height: 869.0)))

            }else{
                print("We're currently don't have this product in stock")
                self.product1.text = "Not available".uppercased()
                SVProgressHUD.showError(withStatus: "We're currently don't have this product in stock")
            }
        }
    }
    @IBAction func drinkAlkalineButton(_ sender: Any) {
        getValues()
        if var alkaline = self.products[0] as? Product{
            let defaults = UserDefaults.standard
            if alkaline.stock_status == "instock"{
                alkaline.quantity += 1
                self.products[0] = alkaline
                defaults.set(try? PropertyListEncoder().encode(products), forKey: "items")
                defaults.synchronize()
                UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(YourBoxMenu(frame: CGRect(x: 42.0, y: 0.0, width: 382.0, height: 869.0)))

            }else{
                print("We're currently don't have this product in stock")
                self.product1.text = "Not available".uppercased()
                SVProgressHUD.showError(withStatus: "We're currently don't have this product in stock")
            }
        }
    }
    
   
    @IBAction func infoButtonAction1(_ sender: Any) {
        let aView = ProductInfoView(frame: CGRect(x: 40, y: 155, width: 310, height:530), types: .water)
        self.view.addSubview(aView)
    }
    
    @IBAction func infoButtonAction2(_ sender: Any) {
        let aView = ProductInfoView(frame: CGRect(x: 40, y: 155, width: 310, height:530), types: .alkaline)
        self.view.addSubview(aView)
    }
}
