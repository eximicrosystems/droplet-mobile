//
//  BrandsViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 01/10/21.
//

import UIKit

class BrandsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }
    
    func addComponents() {
        let navBar = NavigationBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50), imageName: "brands")
        let tabBar = TabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), type: false)
        self.view.addSubview(navBar)
        self.view.addSubview(tabBar)
    }
}
