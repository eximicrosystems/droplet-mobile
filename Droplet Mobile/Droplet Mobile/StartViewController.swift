//
//  ViewController.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla on 21/07/21.
//

import UIKit
import SVProgressHUD

class StartViewController: UIViewController {

    @IBOutlet var loginButton: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var textLogo: UILabel!
    @IBOutlet var logo: UIImageView!
    @IBOutlet var gotas: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        checkState()
    }
    
    func checkState(){
        //Comprobar la sesión del usuario autenticado
        if let data = UserDefaults.standard.value(forKey: "user") as? Data {
            let currentUser = try? PropertyListDecoder().decode(User.self, from: data)
            if currentUser?.billing.phone != nil{
                self.navigationController?.pushViewController(HomeViewController(), animated: false)
            }
        }
    }

    @IBAction func loginButton(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: false)
    }
    
    @IBAction func signupButton(_ sender: Any) {
        self.navigationController?.pushViewController(SignupViewController(), animated: false)
    }
}

