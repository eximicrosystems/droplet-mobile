//
//  Box.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 04/11/21.
//

import Foundation

struct Box: Codable {
    var deliver_to: String?
    var delivery_speed: String?
    var items: [Items]
    var taxes_fee: Float
    var delivery_fee: Float
    var tip_percentage: Float
    var total: Float
}
