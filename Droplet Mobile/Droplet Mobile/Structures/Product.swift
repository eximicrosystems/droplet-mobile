//
//  Product.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation

struct Product: Codable {
    var product_id: Int
    var name: String
    var description: String
    var cost: Int
    var quantity: Int
    var stock_status: String
}
