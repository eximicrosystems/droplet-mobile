//
//  PaymentInformation.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 04/11/21.
//

import Foundation

struct PaymentInformation: Codable {
    var nameOnCard: String
    var numOnCard: String
    var cvv: Int
    var expiration: String
}
