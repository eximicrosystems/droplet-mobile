//
//  ShippingType.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 05/11/21.
//

import Foundation

struct ShippingType: Codable {
    var method_id: String
    var method_title: String
    var total: String
}
