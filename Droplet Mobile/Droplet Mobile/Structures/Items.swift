//
//  Items.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation

struct Items: Codable {
    var product_id: Int
    var quantity: Int
}
