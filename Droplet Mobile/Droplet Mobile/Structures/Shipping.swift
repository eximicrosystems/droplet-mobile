//
//  Shipping.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation
struct Shipping: Codable {
    var first_name: String
    var last_name: String
    var company: String?
    var address_1: String?
    var address_2: String?
    var city: String?
    var state: String?
    var postcode: String?
    var country: String?
}
