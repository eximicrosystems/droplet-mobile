//
//  Order.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation

struct Order: Codable {
    var payment_method: String
    var payment_method_title: String
    var set_paid: Bool
    var billing: Billing
    var shipping: Shipping
    var line_items: [Items]
    var shipping_lines: [ShippingType]
}
