//
//  OrderStatus.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 08/11/21.
//

import Foundation

struct OrderStatus: Codable {
    var id: Int
    var date: String
    var status: String
    var amount: String
}
