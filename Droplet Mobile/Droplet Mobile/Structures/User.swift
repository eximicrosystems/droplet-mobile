//
//  CreateUser.swift
//  Droplet Mobile
//
//  Created by Oscar Sevilla Garduño on 02/11/21.
//

import Foundation

struct User: Codable {
    var id: Int?
    var email: String
    var first_name: String
    var last_name: String
    var username: String?
    var billing: Billing
    var shipping: Shipping
    var cards: [PaymentInformation]?
}
