ADOBE INDESIGN PRINTING INSTRUCTIONS FOR SERVICE PROVIDER REPORT

PUBLICATION NAME: Droplet-Mobile-UX-2021.08.09.indd

PACKAGE DATE: 8/10/21 6:08 PM
Creation Date: 8/10/21
Modification Date: 8/10/21

CONTACT INFORMATION

Company Name: 
Contact: 
Address: 





Phone: 
Fax: 
Email: bill@liquidbold.com

SPECIAL INSTRUCTIONS AND OTHER NOTES






External Plug-ins 0
Non Opaque Objects :On PagePB, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31

FONTS
4 Fonts Used; 0 Missing, 0 Embedded, 0 Incomplete, 0 Protected

Fonts Packaged
- Name: Bryant-Bold; Type: OpenType Type 1, Status: OK
- Name: Bryant-Light; Type: OpenType Type 1, Status: OK
- Name: Bryant-Regular; Type: OpenType Type 1, Status: OK
- Name: MinionPro-Regular; Type: OpenType Type 1, Status: OK


COLORS AND INKS
4 Process Inks; 0 Spot Inks

- Name and Type: Process Cyan; Angle: 75.000; Lines/Inch: 70.000
- Name and Type: Process Magenta; Angle: 15.000; Lines/Inch: 70.000
- Name and Type: Process Yellow; Angle: 0.000; Lines/Inch: 70.000
- Name and Type: Process Black; Angle: 45.000; Lines/Inch: 70.000


LINKS AND IMAGES
(Missing & Embedded Links Only)
Links and Images: 212 Links Found; 0 Modified, 0 Missing, 0 Inaccessible
Images: 0 Embedded, 184 use RGB color space


PRINT SETTINGS
PPD: Device Independent, (PostScript® File)
Printing To: Prepress File
Number of Copies: 1
Reader Spreads: No
Even/Odd Pages: Both
Pages: All
Proof: No
Tiling: None
Scale: 100%, 100%
Page Position: Upper Left
Print Layers: Visible & Printable Layers
Printer's Marks: None
Bleed: 0 px, 0 px, 0 px, 0 px
Color: Composite CMYK
Trapping Mode: None
Send Image Data: All
OPI/DCS Image Replacement: No
Page Size: Custom: 1500 px x 3000 px
Paper Dimensions: 0 px x 0 px
Orientation: Portrait
Negative: No
Flip Mode: Off


FILE PACKAGE LIST

1. Droplet-Mobile-UX-2021.08.09.indd; type: Adobe InDesign publication; size: 12480K
2. Droplet-Mobile-UX-2021.08.09.idml; type: InDesign Markup Document; size: 810K
3. Bryant-Bold.otf; type: Font file; size: 23K
4. Bryant-Light.otf; type: Font file; size: 25K
5. Bryant-Regular.otf; type: Font file; size: 28K
6. MinionPro-Regular.otf; type: Font file; size: 213K
